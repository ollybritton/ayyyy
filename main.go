package main

import (
	"fmt"
	"strings"

	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
)

const responseText = `☜(ﾟヮﾟ☜)

---

I am a bot that noticed your *'\(☞ﾟヮﾟ\)☞'* had gone un☜\(ﾟヮﾟ☜\)ed. This action was performed automatically to help you not feel lonely.`

type reminderBot struct {
	bot reddit.Bot
}

func (r *reminderBot) Comment(c *reddit.Comment) error {
	fmt.Printf("Incoming comment from '%v'\n", c.Author)

	if strings.Contains(c.Body, "(☞ﾟヮﾟ)☞") {
		fmt.Printf("Detected comment from '%v' contains '(☞ﾟヮﾟ)☞'.\n", c.Author)

		for _, subComment := range c.Replies {
			if strings.Contains(subComment.Body, "☜(ﾟヮﾟ☜)") {
				return nil // Stop execution, some idiot has already replied.
			}
		}

		fmt.Printf("Unlonelifying %v\n", c.Author)
		r.bot.Reply(c.Name, responseText)
	}
	return nil
}

func main() {
	fmt.Println("ayyyy-yyyya Starting...")
	if bot, err := reddit.NewBotFromAgentFile("super-secret-keys.yaml", 0); err != nil {
		fmt.Println("Failed to create bot handle: ", err)
	} else {
		cfg := graw.Config{SubredditComments: []string{
			"meirl", "me_irl", "bottesting",
		}}
		handler := &reminderBot{bot: bot}
		if _, wait, err := graw.Run(handler, bot, cfg); err != nil {
			fmt.Println("Failed to start graw run: ", err)
		} else {
			fmt.Println("Bot has started.")
			fmt.Println("graw run failed: ", wait())
		}
	}
}
